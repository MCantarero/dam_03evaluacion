#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>


struct TCordenada{
    double x;
    double y;
};

struct TCordenada Valores (const char *label){

    struct TCordenada recogida;

    printf("Vas a rellenar la %s inicial \n", label);
    printf("Valor de X: ");
    scanf("%lf", &recogida.x);
    printf("\n");
    printf("Valor de Y: ");
    scanf("%lf", &recogida.y);

    return recogida;

}

void Resultado (struct TCordenada distancia_recorrida, double diferencia){

    printf("Distancia en X:%lf Distancia en Y:%lf  diferencia: %lf\n", distancia_recorrida.x, distancia_recorrida.y, diferencia);


}
int main (int argc, char *argy[]){

    struct TCordenada  posicion, velocidad;

    double tb, ta, diferencia;


    posicion = Valores ("Posicion");

    velocidad = Valores ("Velocidad");


    tb = clock();

    printf("ticks antes del bucle %.1lf", tb);

    while (1){

        ta = clock ();

        diferencia = (ta - tb) / CLOCKS_PER_SEC;

        tb = ta;
        //posicion+=velocidad; ESto lo pregunto a chema
        posicion.x += velocidad.x * diferencia;
        posicion.y += velocidad.y * diferencia;
        system("clear");
        Resultado(posicion, diferencia);
        usleep(100000);


    }

    return EXIT_SUCCESS;
}
