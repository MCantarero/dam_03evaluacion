#include <stdio.h>
#include<stdlib.h>

unsigned pedida(){

    unsigned n;

    printf("Introduzca un numero para hacer su factorial: ");

    scanf("%u", &n);

    return n;
}

unsigned factorial(unsigned num){

    if(num < 1 )
        return 1;

    return num * factorial(num - 1);
}
int main (int argc, char *argy[]){

    unsigned numero = pedida();

    printf("%u! = %u \n", numero, factorial(numero));


    return EXIT_SUCCESS;
}
